#include <geom/matrix4x4.hpp>
#include <geom/transform.hpp>
#include <geom/vector3.hpp>

namespace rt
{
  auto transpose(const transform& t) noexcept -> transform { return transform{transpose(t.m_), transpose(t.inv_)}; }
  auto inverse(const transform& t) noexcept -> transform { return transform{t.inv_, t.m_}; }

  auto transform::create_translation(vector3f v) noexcept -> transform
  {
    matrix4x4 m
    {
      1.0f, 0.0f, 0.0f, v.x,
      0.0f, 1.0f, 0.0f, v.y,
      0.0f, 0.0f, 1.0f, v.z,
      0.0f, 0.0f, 0.0f, 1.0f
    };

    matrix4x4 inv
    {
      1.0f, 0.0f, 0.0f, -v.x,
      0.0f, 1.0f, 0.0f, -v.y,
      0.0f, 0.0f, 1.0f, -v.z,
      0.0f, 0.0f, 0.0f, 1.0f
    };

    return transform{m, inv};
  }

  auto transform::create_scale(float x, float y, float z) noexcept -> transform
  {
    matrix4x4 m
    {
      x,    0.0f, 0.0f, 0.0f,
      0.0f, y,    0.0f, 0.0f,
      0.0f, 0.0f, z,    0.0f,
      0.0f, 0.0f, 0.0f, 1.0f
    };

    matrix4x4 inv
    {
      1.0f / x, 0.0f,     0.0f,     0.0f,
      0.0f,     1.0f / y, 0.0f,     0.0f,
      0.0f,     0.0f,     1.0f / z, 0.0f,
      0.0f,     0.0f,     0.0f,     1.0f
    };

    return transform{m, inv};
  }

  auto transform::create_rotation_x(float theta) noexcept -> transform
  {
    auto cos_theta{std::cos(theta)};
    auto sin_theta{std::sin(theta)};

    matrix4x4 m
    {
      1.0f, 0.0f,       0.0f,      0.0f,
      0.0f, cos_theta, -sin_theta, 0.0f,
      0.0f, sin_theta,  cos_theta, 0.0f,
      0.0f, 0.0f,       0.0f,      1.0f
    };

    return transform{m, transpose(m)};
  }

  auto transform::create_rotation_y(float theta) noexcept -> transform
  {
    auto cos_theta{std::cos(theta)};
    auto sin_theta{std::sin(theta)};

    matrix4x4 m
    {
      cos_theta, 0.0f, sin_theta, 0.0f,
      0.0f,      1.0f, 0.0f,      0.0f,
     -sin_theta, 0.0f, cos_theta, 0.0f,
      0.0f,      0.0f, 0.0f,      1.0f
    };

    return transform{m, transpose(m)};
  }

  auto transform::create_rotation_z(float theta) noexcept -> transform
  {
    auto cos_theta{std::cos(theta)};
    auto sin_theta{std::sin(theta)};

    matrix4x4 m
    {
      cos_theta, -sin_theta, 0.0f, 0.0f,
      sin_theta,  cos_theta, 0.0f, 0.0f,
      0.0f,       0.0f,      0.0f, 0.0f,
      0.0f,       0.0f,      0.0f, 1.0f
    };

    return transform{m, transpose(m)};
  }

  auto transform::create_rotation(vector3f axis, float theta) noexcept -> transform
  {
    auto a{normalize(axis)};
    auto cos_theta{std::cos(theta)};
    auto sin_theta{std::sin(theta)};

    matrix4x4 m;

    m(0, 0) = a.x * a.x + (1.0f - a.x * a.x) * cos_theta;
    m(0, 1) = a.x * a.y * (1.0f - cos_theta) - a.z * sin_theta;
    m(0, 2) = a.x * a.z * (1.0f - cos_theta) + a.y * sin_theta;

    m(1, 0) = a.x * a.y * (1.0f - cos_theta) + a.z * sin_theta;
    m(1, 1) = a.y * a.y + (1.0f - a.y * a.y) * cos_theta;
    m(1, 2) = a.y * a.z * (1.0f - cos_theta) - a.x * sin_theta;

    m(2, 0) = a.x * a.z * (1.0f - cos_theta) - a.y * sin_theta;
    m(2, 1) = a.y * a.z * (1.0f - cos_theta) + a.x * sin_theta;
    m(2, 2) = a.z * a.z + (1.0f - a.z * a.z) * cos_theta;

    return transform{m, transpose(m)};
  }

  transform::transform(const matrix4x4& m) noexcept : m_{m}, inv_{inverse(m_)} {}
  transform::transform(const matrix4x4& m, const matrix4x4& inv) noexcept : m_{m}, inv_{inv} {}
}