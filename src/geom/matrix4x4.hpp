#ifndef GEOM_MATRIX4X4_HPP
#define GEOM_MATRIX4X4_HPP

#include <cassert>
#include <cmath>

#include <utility>

#include <gsl/gsl>

namespace rt
{
  class matrix4x4
  {
  public:
    constexpr matrix4x4() noexcept = default;
    
    constexpr matrix4x4(float m[4][4]) noexcept
    {
      for (auto i = 0; i < 4; ++i)
        for (auto j = 0; j < 4; ++j)
          m_[i][j] = m[i][j];
    }

    constexpr matrix4x4
    (
      float m00, float m01, float m02, float m03,
      float m10, float m11, float m12, float m13,
      float m20, float m21, float m22, float m23,
      float m30, float m31, float m32, float m33
    )
    noexcept
    {
      m_[0][0] = m00;
      m_[0][1] = m01;
      m_[0][2] = m02;
      m_[0][3] = m03;

      m_[1][0] = m10;
      m_[1][1] = m11;
      m_[1][2] = m12;
      m_[1][3] = m13;

      m_[2][0] = m20;
      m_[2][1] = m21;
      m_[2][2] = m22;
      m_[2][3] = m23;

      m_[3][0] = m30;
      m_[3][1] = m31;
      m_[3][2] = m32;
      m_[3][3] = m33;
    }

    constexpr auto operator()(gsl::index i, gsl::index j) const noexcept
    {
      Expects(0 <= i && i < 4);
      Expects(0 <= j && j < 4);
      return m_[i][j];
    }

    constexpr auto& operator()(gsl::index i, gsl::index j) noexcept
    {
      Expects(0 <= i && i < 4);
      Expects(0 <= j && j < 4);
      return m_[i][j];
    }
  private:
    float m_[4][4]
    {
      {1.0f, 0.0f, 0.0f, 0.0f},
      {0.0f, 1.0f, 0.0f, 0.0f},
      {0.0f, 0.0f, 1.0f, 0.0f},
      {0.0f, 0.0f, 0.0f, 1.0f}
    };
  };

  constexpr auto operator==(const matrix4x4& m1, const matrix4x4& m2) noexcept
  {
    for (auto i{0}; i < 4; ++i)
      for (auto j{0}; j < 4; ++j)
        if (m1(i, j) != m2(i, j))
          return false;
    
    return true;
  }

  constexpr auto operator!=(const matrix4x4& m1, const matrix4x4& m2) noexcept
  {
    return !(m1 == m2);
  }

  constexpr auto transpose(const matrix4x4& m) noexcept
  {
    return matrix4x4
    {
      m(0, 0), m(1, 0), m(2, 0), m(3, 0),
      m(0, 1), m(1, 1), m(2, 1), m(3, 1),
      m(0, 2), m(1, 2), m(2, 2), m(3, 2),
      m(0, 3), m(1, 3), m(2, 3), m(3, 3)
    };
  }

  inline auto inverse(const matrix4x4& m) noexcept
  {
    gsl::index row_indices[4];
    gsl::index col_indices[4];
    gsl::index pivot_indices[4]{0};

    matrix4x4 ret{m};

    for (auto i{0}; i < 4; ++i)
    {
      auto row_index{0};
      auto col_index{0};
      auto greatest{0.0f};

      for (auto j{0}; j < 4; ++j)
      {
        if (pivot_indices[j] != 1)
        {
          for (auto k{0}; k < 4; ++k)
          {
            assert(pivot_indices[k] <= 1);
            if (pivot_indices[k] == 0)
            {
              if (auto candidate{std::abs(ret(j, k))}; candidate >= greatest)
              {
                greatest = candidate;
                row_index = j;
                col_index = k;
              }
            }
          }
        }
      }

      ++pivot_indices[col_index];

      if (row_index != col_index)
        for (auto j{0}; j < 4; ++j)
          std::swap(ret(row_index, j), ret(col_index, j));
      
      row_indices[i] = row_index;
      col_indices[i] = col_index;

      auto& pivot{ret(col_index, col_index)};
      assert(pivot != 0.0f);

      auto pivot_inv{1.0f / pivot};
      pivot = 1.0f;

      for (auto j{0}; j < 4; ++j)
        ret(col_index, j) *= pivot_inv;
      
      for (auto j{0}; j < 4; ++j)
      {
        if (j != col_index)
        {
          auto save{ret(j, col_index)};
          ret(j, col_index) = 0.0f;

          for (auto k{0}; k < 4; ++k)
            ret(j, k) -= ret(col_index, k) * save;
        }
      }
    }

    for (auto i{3}; i >= 0; --i)
      if (row_indices[i] != col_indices[i])
        for (auto j{0}; j < 4; ++j)
          std::swap(ret(j, row_indices[i]), ret(j, col_indices[i]));
      
    return ret;
  }

  constexpr auto operator*(const matrix4x4& m1, const matrix4x4& m2) noexcept
  {
    matrix4x4 ret;

    for (auto i{0}; i < 4; ++i) 
      for (auto j{0}; j < 4; ++j)
        ret(i, j) =
          m1(i, 0) * m2(0, j) +
          m1(i, 1) * m2(1, j) +
          m1(i, 2) * m2(2, j) +
          m1(i, 3) * m2(3, j);

    return ret;
  }

  constexpr auto operator*=(matrix4x4& m1, const matrix4x4& m2) noexcept
  {
    matrix4x4 ret;

    for (auto i{0}; i < 4; ++i) 
      for (auto j{0}; j < 4; ++j)
        ret(i, j) =
          m1(i, 0) * m2(0, j) +
          m1(i, 1) * m2(1, j) +
          m1(i, 2) * m2(2, j) +
          m1(i, 3) * m2(3, j);

    return m1 = ret;
  }
}

#endif