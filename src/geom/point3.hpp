#ifndef GEOM_POINT3_HPP
#define GEOM_POINT3_HPP

#include <cmath>

#include <algorithm>
#include <type_traits>

#include <gsl/gsl>

#include <geom/utility.hpp>
#include <geom/vector3.hpp>

namespace rt
{
  template <typename T>
  struct point3
  {
    static_assert(std::is_arithmetic_v<T>, "T must be an arithmetic type in point3<T>");

    T x{0};
    T y{0};
    T z{0};

    constexpr point3() noexcept = default;
    constexpr explicit point3(T x) noexcept : x{x}, y{x}, z{x} {}
    constexpr point3(T x, T y, T z) noexcept : x{x}, y{y}, z{z} {}

    template <typename U>
    constexpr explicit operator point3<U>() const noexcept
    {
      return point3<U>
      {
        static_cast<U>(x),
        static_cast<U>(y),
        static_cast<U>(z)
      };
    }

    constexpr auto operator[](gsl::index i) const noexcept
    {
      Expects(0 <= i && i < 3);

      if (i == 0)
        return x;
      if (i == 1)
        return y;
    
      return z;
    }

    constexpr auto& operator[](gsl::index i) noexcept
    {
      Expects(0 <= i && i < 3);

      if (i == 0)
        return x;
      if (i == 1)
        return y;
        
      return z;
    }
  };

  using point3f = point3<float>;
  using point3i = point3<int>;

  template <typename T>
  constexpr auto to_point(vector3<T> v) noexcept
  {
    return point3<T>{v.x, v.y, v.z};
  }

  template <typename T>
  constexpr auto to_vector(point3<T> p) noexcept
  {
    return vector3<T>{p.x, p.y, p.z};
  }

  template <typename T>
  constexpr auto operator==(point3<T> p1, point3<T> p2) noexcept
  {
    return p1.x == p2.x && p1.y == p2.y && p1.z == p2.z;
  }

  template <typename T>
  constexpr auto operator!=(point3<T> p1, point3<T> p2) noexcept
  {
    return !(p1 == p2);
  }

  template <typename T, typename U>
  constexpr auto operator+(vector3<T> v, point3<U> p) noexcept
  {
    using result_t = decltype(v.x + p.x);
    return point3<result_t>{v.x + p.x, v.y + p.y, v.z + p.z};
  }

  template <typename T, typename U>
  constexpr auto operator+(point3<T> p, vector3<U> v) noexcept
  {
    using result_t = decltype(p.x + v.x);
    return point3<result_t>{p.x + v.x, p.y + v.y, p.z + v.z};
  }

  template <typename T, typename U>
  constexpr auto& operator+=(point3<T>& p, vector3<U> v) noexcept
  {
    p.x += v.x;
    p.y += v.y;
    p.z += v.z;
    return p;
  }

  template <typename T, typename U>
  constexpr auto operator-(point3<T> p, vector3<U> v) noexcept
  {
    using result_t = decltype(p.x - v.x);
    return point3<result_t>{p.x - v.x, p.y - v.y, p.z - v.z};
  }

  template <typename T, typename U>
  constexpr auto& operator-=(point3<T>& p, vector3<U> v) noexcept
  {
    p.x -= v.x;
    p.y -= v.y;
    p.z -= v.z;
    return p;
  }

  template <typename T, typename U>
  constexpr auto operator-(point3<T> p1, point3<U> p2) noexcept
  {
    using result_t = decltype(p1.x - p2.x);
    return vector3<result_t>{p1.x - p2.x, p1.y - p2.y, p1.z - p2.z};
  }

  template <typename T, typename U>
  constexpr auto distance_sq(point3<T> p1, point3<U> p2) noexcept 
  {
    return length_sq(p1 - p2);
  }

  template <typename T, typename U>
  inline auto distance(point3<T> p1, point3<U> p2) noexcept
  {
    return length(p1 - p2);
  }

  template <typename T>
  constexpr auto lerp(point3<T> p1, point3<T> p2, float t) noexcept 
  {
    return point3<T>
    {
      lerp(p1.x, p2.x, t),
      lerp(p1.y, p2.y, t),
      lerp(p1.z, p2.z, t)
    };
  }

  template <typename T>
  constexpr auto midpoint(point3<T> p1, point3<T> p2) noexcept
  {
    return point3<T>
    {
      (p1.x + p2.x) / 2,
      (p1.y + p2.y) / 2,
      (p1.z + p2.z) / 2
    };
  }

  template <typename T>
  inline auto floor(point3<T> p) noexcept
  {
    return point3<T>{std::floor(p.x), std::floor(p.y), std::floor(p.z)};
  }
  
  template <typename T>
  inline auto ceil(point3<T> p) noexcept
  {
    return point3<T>{std::ceil(p.x), std::ceil(p.y), std::ceil(p.z)};
  }

  template <typename T>
  inline auto abs(point3<T> p) noexcept
  {
    return point3<T>{std::abs(p.x), std::abs(p.y), std::abs(p.z)};
  }

  template <typename T>
  constexpr auto min_component(point3<T> p) noexcept
  {
    return std::min(p.x, std::min(p.y, p.z));
  }

  template <typename T>
  constexpr auto max_component(point3<T> p) noexcept
  {
    return std::max(p.x, std::max(p.y, p.z));
  }

  template <typename T>
  constexpr auto min_index(point3<T> p) noexcept
  {
    return p.x < p.y ? (p.x < p.z ? 0 : 2) : (p.y < p.z ? 1 : 2);
  }

  template <typename T>
  constexpr auto max_index(point3<T> p) noexcept
  {
    return p.x > p.y ? (p.x > p.z ? 0 : 2) : (p.y > p.z ? 1 : 2);
  }

  template <typename T>
  constexpr auto min(point3<T> p1, point3<T> p2) noexcept
  {
    return point3<T>{std::min(p1.x, p2.x), std::min(p1.y, p2.y), std::min(p1.z, p2.z)};
  }

  template <typename T>
  constexpr auto max(point3<T> p1, point3<T> p2) noexcept
  {
    return point3<T>{std::max(p1.x, p2.x), std::max(p1.y, p2.y), std::max(p1.z, p2.z)};
  }

  template <typename T>
  constexpr auto permute(point3<T> p, gsl::index x, gsl::index y, gsl::index z) noexcept
  {
    return point3<T>{p[x], p[y], p[z]};
  }
}

#endif