#ifndef GEOM_BOUNDS3_HPP
#define GEOM_BOUNDS3_HPP

#include <type_traits>
#include <utility>

#include <geom/point3.hpp>
#include <geom/utility.hpp>
#include <geom/vector3.hpp>
#include <util/constants.hpp>

namespace rt
{
  template <typename T>
  struct bounds3
  {
    static_assert(std::is_arithmetic_v<T>, "T must be an arithmetic type in bounds3<T>");
    
    class iterator
    {
      static_assert(std::is_integral_v<T>, "T must be an integral type in bounds3<T>::iterator");
    public:
      inline iterator(const bounds3<T>& bounds, point3<T> offset) noexcept : bounds_{&bounds}, offset_{offset} {}

      inline friend auto operator==(const iterator& i1, const iterator& i2) noexcept
      {
        return i1.bounds_ == i2.bounds_ && i1.offset_ == i2.offset_;
      }

      inline friend auto operator!=(const iterator& i1, const iterator& i2) noexcept
      {
        return !(i1 == i2);
      }

      inline auto operator*() const noexcept
      {
        return offset_;
      }

      inline auto& operator++() noexcept
      {
        if (++offset_.x == bounds_->p_max.x)
        {
          offset_.x = bounds_->p_min.x;
          
          if (++offset_.y == bounds_->p_max.y)
          {
            offset_.y = bounds_->p_min.y;
            offset_.z++;
          }
        }

        return *this;
      }
    private:
      const bounds3<T>* bounds_{nullptr};
      point3<T> offset_;
    };

    point3<T> p_min{max_val<T>};
    point3<T> p_max{low_val<T>};

    constexpr bounds3() noexcept = default;
    constexpr explicit bounds3(point3<T> p) noexcept : p_min{p}, p_max{p} {}
    constexpr bounds3(point3<T> p1, point3<T> p2) noexcept : p_min{min(p1, p2)}, p_max{max(p1, p2)} {}

    constexpr auto operator[](gsl::index i) const noexcept
    {
      Expects(0 <= i && i < 2);

      if (i == 0)
        return p_min;
      else
        return p_max;
    }

    constexpr auto& operator[](gsl::index i) noexcept
    {
      Expects(0 <= i && i < 2);

      if (i == 0)
        return p_min;
      else
        return p_max;
    }

    inline auto begin() const noexcept
    {
      return iterator{*this, p_min};
    }

    inline auto end() const noexcept
    {
      return iterator{*this, {p_min.x, p_min.y, p_max.z}};
    }
  };

  using bounds3f = bounds3<float>;
  using bounds3i = bounds3<int>;

  template <typename T>
  constexpr auto operator==(const bounds3<T>& b1, const bounds3<T>& b2) noexcept
  {
    return b1.p_min == b2.p_min && b1.p_max == b2.p_max;
  }

  template <typename T>
  constexpr auto operator!=(const bounds3<T>& b1, const bounds3<T>& b2) noexcept
  {
    return !(b1 == b2);
  }

  template <typename T>
  constexpr auto corner(const bounds3<T>& b, gsl::index i) noexcept
  {
    Expects(0 <= i && i < 8);
    return point3
    {
      b[i >> 0 & 1].x,
      b[i >> 1 & 1].y,
      b[i >> 2 & 1].z
    };
  }

  template <typename T>
  constexpr auto union_of(const bounds3<T>& b, point3<T> p) noexcept
  {
    return bounds3
    {
      min(b.p_min, p),
      max(b.p_max, p)
    };
  }

  template <typename T>
  constexpr auto union_of(const bounds3<T>& b1, const bounds3<T>& b2) noexcept
  {
    return bounds3
    {
      min(b1.p_min, b2.p_min),
      max(b1.p_max, b2.p_max)
    };
  }

  template <typename T>
  constexpr auto intersects(const bounds3<T>& b1, const bounds3<T>& b2) noexcept
  {
    return
      b1.p_max.x >= b2.p_min.x && b1.p_min.x <= b2.p_max.x &&
      b1.p_max.y >= b2.p_min.y && b1.p_min.y <= b2.p_max.y &&
      b1.p_max.z >= b2.p_min.z && b1.p_min.z <= b2.p_max.z;
  }

  template <typename T>
  constexpr auto intersect_of(const bounds3<T>& b1, const bounds3<T>& b2) noexcept
  {
    Expects(intersects(b1, b2));

    return bounds3
    {
      max(b1.p_min, b2.p_min),
      min(b1.p_max, b2.p_max)
    };
  }

  template <typename T>
  constexpr auto inside_inclusive(const bounds3<T>& b, point3<T> p) noexcept
  {
    return 
      b.p_min.x <= p.x && p.x <= b.p_max.x &&
      b.p_min.y <= p.y && p.y <= b.p_max.y &&
      b.p_min.z <= p.z && p.z <= b.p_max.z;
  }

  template <typename T>
  constexpr auto inside_exclusive(const bounds3<T>& b, point3<T> p) noexcept
  {
    return 
      b.p_min.x <= p.x && p.x < b.p_max.x &&
      b.p_min.y <= p.y && p.y < b.p_max.y &&
      b.p_min.z <= p.z && p.z < b.p_max.z;
  }

  template <typename T>
  constexpr auto grow(const bounds3<T>& b, T delta) noexcept
  {
    vector3 vec{delta};
    return bounds3
    {
      b.p_min - delta,
      b.p_max + delta
    };
  }

  template <typename T>
  constexpr auto diagonal(const bounds3<T>& b) noexcept
  {
    return b.p_max - b.p_min;
  }

  template <typename T>
  constexpr auto area(const bounds3<T>& b) noexcept
  {
    auto d{diagonal(b)};
    return 2 * (d.x * d.y + d.x * d.z + d.y * d.z);
  }

  template <typename T>
  constexpr auto volume(const bounds3<T>& b) noexcept
  {
    auto d{diagonal(b)};
    return d.x * d.y * d.z;
  }

  template <typename T>
  constexpr auto min_component(const bounds3<T>& b) noexcept
  {
    return min_component(diagonal(b));
  }

  template <typename T>
  constexpr auto max_component(const bounds3<T>& b) noexcept
  {
    return max_component(diagonal(b));
  }

  template <typename T>
  constexpr auto min_dimension(const bounds3<T>& b) noexcept
  {
    return min_dimension(diagonal(b));
  }

  template <typename T>
  constexpr auto max_dimension(const bounds3<T>& b) noexcept
  {
    return max_dimension(diagonal(b));
  }

  template <typename T>
  constexpr auto lerp(const bounds3<T>& b, point3f t) noexcept
  {
    return point3
    {
      lerp(b.p_min.x, b.p_max.x, t.x),
      lerp(b.p_min.y, b.p_max.y, t.y),
      lerp(b.p_min.z, b.p_max.z, t.z)
    };
  }

  template <typename T>
  constexpr auto lerp_inv(const bounds3<T>& b, point3<T> p) noexcept
  {
    return point3f
    {
      lerp_inv(b.p_min.x, b.p_max.x, p.x),
      lerp_inv(b.p_min.y, b.p_max.y, p.y),
      lerp_inv(b.p_min.z, b.p_max.z, p.z)
    };
  }

  template <typename T>
  inline auto bounding_sphere(const bounds3<T>& b) noexcept
  {
    auto center{midpoint(static_cast<point3f>(b.p_min), static_cast<point3f>(b.p_max))};
    auto radius{distance(center, b.p_max)};
    return std::pair{center, radius};
  }
}

#endif