#ifndef GEOM_TRANSFORM_HPP
#define GEOM_TRANSFROM_HPP

#include <geom/matrix4x4.hpp>
#include <geom/vector3.hpp>

namespace rt
{
  class transform
  {
  public:
    friend auto transpose(const transform& t) noexcept -> transform;
    friend auto inverse(const transform& t) noexcept -> transform;

    static auto create_translation(vector3f v) noexcept -> transform;
    
    static auto create_scale(float x, float y, float z) noexcept -> transform;

    static auto create_rotation_x(float theta) noexcept -> transform;
    static auto create_rotation_y(float theta) noexcept -> transform;
    static auto create_rotation_z(float theta) noexcept -> transform;
    static auto create_rotation(vector3f axis, float theta) noexcept -> transform;

    explicit transform(const matrix4x4& m) noexcept;
    transform(const matrix4x4& m, const matrix4x4& inv) noexcept;
  private:
    matrix4x4 m_;
    matrix4x4 inv_;
  };

  auto transpose(const transform& t) noexcept -> transform;
  auto inverse(const transform& t) noexcept -> transform;
}

#endif