#ifndef GEOM_VECTOR3_HPP
#define GEOM_VECTOR3_HPP

#include <cmath>

#include <algorithm>
#include <type_traits>

#include <gsl/gsl>

namespace rt
{
  template <typename T>
  struct vector3
  {
    static_assert(std::is_arithmetic_v<T>, "T must be an arithmetic type in vector3<T>");

    T x{0};
    T y{0};
    T z{0};

    constexpr vector3() noexcept = default;
    constexpr explicit vector3(T x) noexcept : x{x}, y{x}, z{x} {}
    constexpr vector3(T x, T y, T z) noexcept : x{x}, y{y}, z{z} {}

    template <typename U>
    constexpr explicit operator vector3<U>() const noexcept
    {
      return vector3
      {
        static_cast<U>(x),
        static_cast<U>(y),
        static_cast<U>(z)
      };
    }

    constexpr auto operator[](gsl::index i) const noexcept
    {
      Expects(0 <= i && i < 3);

      if (i == 0)
        return x;
      if (i == 1)
        return y;
      
      return z;
    }

    constexpr auto& operator[](gsl::index i) noexcept
    {
      Expects(0 <= i && i < 3);
      
      if (i == 0)
        return x;
      if (i == 1)
        return y;

      return z;
    }
  };

  using vector3f = vector3<float>;
  using vector3i = vector3<int>;

  template <typename T>
  constexpr auto operator==(vector3<T> v1, vector3<T> v2) noexcept
  {
    return v1.x == v2.x && v1.y == v2.y && v1.z == v2.z;
  }

  template <typename T>
  constexpr auto operator!=(vector3<T> v1, vector3<T> v2) noexcept
  {
    return !(v1 == v2);
  }

  template <typename T, typename U>
  constexpr auto operator+(vector3<T> v1, vector3<U> v2) noexcept
  {
    return vector3{v1.x + v2.x, v1.y + v2.y, v1.z + v2.z};
  }

  template <typename T, typename U>
  constexpr auto& operator+=(vector3<T>& v1, vector3<U> v2) noexcept
  {
    v1.x += v2.x;
    v1.y += v2.y;
    v1.z += v2.z;
    return v1;
  }

  template <typename T>
  constexpr auto operator-(vector3<T> v1, vector3<T> v2) noexcept
  {
    return vector3{v1.x - v2.x, v1.y - v2.y, v1.z - v2.z};
  }

  template <typename T, typename U>
  constexpr auto& operator-=(vector3<T>& v1, vector3<U> v2) noexcept
  {
    v1.x -= v2.x;
    v1.y -= v2.y;
    v1.z -= v2.z;
    return v1;
  }

  template <typename T, typename U>
  constexpr auto operator*(T s, vector3<U> v) noexcept
  {
    return vector3{s * v.x, s * v.y, s * v.z};
  }

  template <typename T, typename U>
  constexpr auto operator*(vector3<T> v, U s) noexcept
  {
    return vector3{v.x * s, v.y * s, v.z * s};
  }

  template <typename T, typename U>
  constexpr auto& operator*=(vector3<T>& v, U s) noexcept
  {
    v.x *= s;
    v.y *= s;
    v.z *= s;
    return v;
  }

  template <typename T, typename U>
  constexpr auto operator/(vector3<T> v, U s) noexcept
  {
    Expects(s != 0);

    using result_t = decltype(v.x / s);

    if constexpr (std::is_integral_v<result_t>)
      return vector3{v.x / s, v.y / s, v.z / s};

    if constexpr (std::is_floating_point_v<result_t>)
    {
      auto inv{1.0f / s};
      return vector3{v.x * inv, v.y * inv, v.z * inv};
    }
  }

  template <typename T, typename U>
  constexpr auto& operator/=(vector3<T>& v, U s) noexcept
  {
    Expects(s != 0);
    
    if constexpr (std::is_integral_v<T>)
    {
      v.x /= s;
      v.y /= s;
      v.z /= s;
      return v;
    }

    if constexpr (std::is_floating_point_v<T>)
    {
      auto inv{1.0f / s};
      v.x *= inv;
      v.y *= inv;
      v.z *= inv;
      return v;
    }
  }

  template <typename T>
  constexpr auto operator-(vector3<T> v) noexcept
  {
    return vector3{-v.x, -v.y, -v.z};
  }

  template <typename T>
  inline auto abs(vector3<T> v) noexcept
  {
    return vector3{std::abs(v.x), std::abs(v.y), std::abs(v.z)};
  }

  template <typename T, typename U>
  constexpr auto dot(vector3<T> v1, vector3<U> v2) noexcept
  {
    return v1.x * v2.x + v1.y * v2.y + v1.z * v2.z;
  }

  template <typename T, typename U>
  constexpr auto cross(vector3<T> v1, vector3<U> v2) noexcept
  {
    using result_t = decltype(v1.x * v2.x);

    if constexpr (std::is_integral_v<result_t>)
    {
      return vector3
      {
        v1.y * v2.z - v1.z * v2.y,
        v1.z * v2.x - v1.x * v2.z,
        v1.x * v2.y - v1.y * v2.x
      };
    }
    
    if constexpr (std::is_floating_point_v<result_t>)
    {
      double v1x{v1.x};
      double v1y{v1.y};
      double v1z{v1.z};
      double v2x{v2.x};
      double v2y{v2.y};
      double v2z{v2.z};

      return vector3
      {
        static_cast<result_t>(v1y * v2z - v1z * v2y),
        static_cast<result_t>(v1z * v2x - v1x * v2z),
        static_cast<result_t>(v1x * v2y - v1y * v2x)
      };
    }
  }

  template <typename T>
  constexpr auto length_sq(vector3<T> v) noexcept
  {
    return v.x * v.x + v.y * v.y + v.z * v.z;
  }

  template <typename T>
  inline auto length(vector3<T> v) noexcept
  {
    return std::sqrt(static_cast<float>(length_sq(v)));
  }

  template <typename T>
  inline auto normalize(vector3<T> v) noexcept
  {
    return v / length(v);
  }

  template <typename T>
  constexpr auto min_component(vector3<T> v) noexcept
  {
    return std::min(v.x, std::min(v.y, v.z));
  }

  template <typename T>
  constexpr auto max_component(vector3<T> v) noexcept
  {
    return std::max(v.x, std::max(v.y, v.z));
  }

  template <typename T>
  constexpr auto min_index(vector3<T> v) noexcept
  {
    return v.x < v.y ? (v.x < v.z ? 0 : 2) : (v.y < v.z ? 1 : 2);
  }
  
  template <typename T>
  constexpr auto max_index(vector3<T> v) noexcept
  {
    return v.x > v.y ? (v.x > v.z ? 0 : 2) : (v.y > v.z ? 1 : 2);
  }

  template <typename T>
  constexpr auto min(vector3<T> v1, vector3<T> v2) noexcept
  {
    return vector3{std::min(v1.x, v2.x), std::min(v1.y, v2.y), std::min(v1.z, v2.z)};
  }

  template <typename T>
  constexpr auto max(vector3<T> v1, vector3<T> v2) noexcept
  {
    return vector3{std::max(v1.x, v2.x), std::max(v1.y, v2.y), std::max(v1.z, v2.z)};
  }

  template <typename T>
  constexpr auto permute(vector3<T> v, gsl::index x, gsl::index y, gsl::index z) noexcept
  {
    return vector3{v[x], v[y], v[z]};
  }

  template <typename T>
  inline auto coord_system(vector3<T> v) noexcept
  {
    if (std::abs(v.x) > std::abs(v.y))
    {
      auto v1{normalize(vector3{-v.z, 0, v.x})};
      auto v2{cross(v, v1)};

      return std::pair{v1, v2};
    }
    else
    {
      auto v1{normalize(vector3{0, v.z, -v.y})};
      auto v2{cross(v, v1)};

      return std::pair{v1, v2};
    }
  }
}

#endif