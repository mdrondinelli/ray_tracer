#ifndef GEOM_POINT2_HPP
#define GEOM_POINT2_HPP

#include <cmath>

#include <algorithm>
#include <type_traits>

#include <gsl/gsl>

#include <geom/utility.hpp>
#include <geom/vector2.hpp>

namespace rt
{
  template <typename T>
  struct point2
  {
    static_assert(std::is_arithmetic_v<T>, "T must be an arithmetic type in point2<T>");

    T x{0};
    T y{0};

    constexpr point2() noexcept = default;
    constexpr explicit point2(T x) noexcept : x{x}, y{x} {}
    constexpr point2(T x, T y) noexcept : x{x}, y{y} {}

    template <typename U>
    constexpr explicit operator point2<U>() const noexcept
    {
      return point2<U>
      {
        static_cast<U>(x),
        static_cast<U>(y)
      };
    }

    constexpr auto operator[](gsl::index i) const noexcept
    {
      Expects(0 <= i && i < 2);

      if (i == 0)
        return x;
      else
        return y;
    }

    constexpr auto& operator[](gsl::index i) noexcept
    {
      Expects(0 <= i && i < 2);

      if (i == 0)
        return x;
      else
        return y;
    }
  };

  using point2f = point2<float>;
  using point2i = point2<int>;

  template <typename T>
  constexpr auto to_point(vector2<T> v) noexcept
  {
    return point2<T>{v.x, v.y};
  }

  template <typename T>
  constexpr auto to_vector(point2<T> p) noexcept
  {
    return vector2<T>{p.x, p.y};
  }

  template <typename T>
  constexpr auto operator==(point2<T> p1, point2<T> p2) noexcept
  {
    return p1.x == p2.x && p1.y == p2.y;
  }

  template <typename T>
  constexpr auto operator!=(point2<T> p1, point2<T> p2) noexcept
  {
    return !(p1 == p2);
  }

  template <typename T, typename U>
  constexpr auto operator+(vector2<T> v, point2<U> p) noexcept
  {
    using result_t = decltype(v.x + p.x);
    return point2<result_t>{v.x + p.x, v.y + p.y};
  }

  template <typename T, typename U>
  constexpr auto operator+(point2<T> p, vector2<U> v) noexcept
  {
    using result_t = decltype(p.x + v.x);
    return point2<result_t>{p.x + v.x, p.y + v.y};
  }

  template <typename T, typename U>
  constexpr auto& operator+=(point2<T>& p, vector2<U> v) noexcept
  {
    p.x += v.x;
    p.y += v.y;
    return p;
  }

  template <typename T, typename U>
  constexpr auto operator-(point2<T> p, vector2<U> v) noexcept
  {
    using result_t = decltype(p.x - v.x);
    return point2<result_t>{p.x - v.x, p.y - v.y};
  }

  template <typename T, typename U>
  constexpr auto& operator-=(point2<T>& p, vector2<U> v) noexcept
  {
    p.x -= v.x;
    p.y -= v.y;
    return p;
  }

  template <typename T, typename U>
  constexpr auto operator-(point2<T> p1, point2<U> p2) noexcept
  {
    using result_t = decltype(p1.x - p2.x);
    return vector2<result_t>{p1.x - p2.x, p1.y - p2.y};
  }

  template <typename T, typename U>
  constexpr auto distance_sq(point2<T> p1, point2<U> p2) noexcept 
  {
    return length_sq(p1 - p2);
  }

  template <typename T, typename U>
  inline auto distance(point2<T> p1, point2<U> p2) noexcept
  {
    return length(p1 - p2);
  }

  template <typename T>
  constexpr auto lerp(point2<T> p1, point2<T> p2, float t) noexcept 
  {
    return point2<T>
    {
      lerp(p1.x, p2.x, t),
      lerp(p1.y, p2.y, t)
    };
  }

  template <typename T>
  constexpr auto midpoint(point2<T> p1, point2<T> p2) noexcept
  {
    return point2<T>
    {
      (p1.x + p2.x) / 2,
      (p1.y + p2.y) / 2
    };
  }

  template <typename T>
  inline auto floor(point2<T> p) noexcept
  {
    return point2<T>{std::floor(p.x), std::floor(p.y)};
  }
  
  template <typename T>
  inline auto ceil(point2<T> p) noexcept
  {
    return point2<T>{std::ceil(p.x), std::ceil(p.y)};
  }

  template <typename T>
  inline auto abs(point2<T> p) noexcept
  {
    return point2<T>{std::abs(p.x), std::abs(p.y)};
  }

  template <typename T>
  constexpr auto min_component(point2<T> p) noexcept
  {
    return std::min(p.x, p.y);
  }

  template <typename T>
  constexpr auto max_component(point2<T> p) noexcept
  {
    return std::max(p.x, p.y);
  }

  template <typename T>
  constexpr auto min_dimension(point2<T> p) noexcept
  {
    return p.x < p.y ? 0 : 1;
  }

  template <typename T>
  constexpr auto max_dimension(point2<T> p) noexcept
  {
    return p.x > p.y ? 0 : 1;
  }

  template <typename T>
  constexpr auto min(point2<T> p1, point2<T> p2) noexcept
  {
    return point2<T>{std::min(p1.x, p2.x), std::min(p1.y, p2.y)};
  }

  template <typename T>
  constexpr auto max(point2<T> p1, point2<T> p2) noexcept
  {
    return point2<T>{std::max(p1.x, p2.x), std::max(p1.y, p2.y)};
  }

  template <typename T>
  constexpr auto permute(point2<T> p, gsl::index x, gsl::index y) noexcept
  {
    return point2<T>{p[x], p[y]};
  }
}

#endif