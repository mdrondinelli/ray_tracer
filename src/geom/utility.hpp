#ifndef GEOM_UTILITY_HPP
#define GEOM_UTILITY_HPP

#include <cmath>

#include <optional>
#include <utility>

namespace rt
{
  template <typename T>
  constexpr auto lerp(T lo, T hi, float t) noexcept
  {
    return static_cast<T>(lo * (1.0f - t) + hi * t);
  }

  template <typename T>
  constexpr auto lerp_inv(T lo, T hi, T mid) noexcept
  {
    auto delta{static_cast<float>(hi - lo)};
    auto offset{static_cast<float>(mid - lo)};
    return offset / delta;
  }

  inline auto solve_quadratic(float a, float b, float c) noexcept -> std::optional<std::pair<float, float>>
  {
    double radicand{b * b - 4.0 * a * c};
    if (radicand < 0)
      return std::nullopt;

    double radical{std::sqrt(radicand)};
    double q{b < 0 ? -0.5 * (b - radical) : -0.5 * (b + radical)};

    auto x0{static_cast<float>(q / a)};
    auto x1{static_cast<float>(c / q)};

    if (x0 > x1)
      std::swap(x0, x1);
    
    return std::make_pair(x0, x1);
  }
}

#endif