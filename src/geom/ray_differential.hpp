#ifndef GEOM_RAY_DIFFERENTIAL_HPP
#define GEOM_RAY_DIFFERENTIAL_HPP

#include <geom/point3.hpp>
#include <geom/vector3.hpp>
#include <geom/ray.hpp>
#include <util/constants.hpp>

namespace rt
{
  struct ray_differential : public ray
  {
    bool has_differentials{false};

    point3f rx_origin;
    point3f ry_origin;

    vector3f rx_direction;
    vector3f ry_direction;

    constexpr ray_differential() noexcept = default;
    constexpr ray_differential(const ray& r) noexcept
      : ray{r} {}
    constexpr ray_differential(point3f o, vector3f d, float tmax = inf_val<float>, float time = 0.0f) noexcept
      : ray{o, d, tmax, time} {}
  };

  constexpr auto operator==(const ray_differential& r1, const ray_differential& r2) noexcept
  {
    const ray& r1_base{r1};
    const ray& r2_base{r2};

    return
      r1.has_differentials == r2.has_differentials &&
      r1_base == r2_base &&
      r1.rx_origin == r2.rx_origin &&
      r1.ry_origin == r2.ry_origin &&
      r1.rx_direction == r2.rx_direction &&
      r1.ry_direction == r2.ry_direction;
  }

  constexpr auto operator!=(const ray_differential& r1, const ray_differential& r2) noexcept
  {
    return !(r1 == r2);
  }

  constexpr auto scale_differentials(ray_differential& r, float s) noexcept
  {
    r.rx_origin = r.o + (r.rx_origin - r.o) * s;
    r.ry_origin = r.o + (r.ry_origin - r.o) * s;

    r.rx_direction = r.d + (r.rx_direction - r.d) * s;
    r.ry_direction = r.d + (r.ry_direction - r.d) * s;
  }
}

#endif