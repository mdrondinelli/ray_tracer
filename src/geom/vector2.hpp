#ifndef GEOM_VECTOR2_HPP
#define GEOM_VECTOR2_HPP

#include <cmath>

#include <algorithm>
#include <type_traits>

#include <gsl/gsl>

namespace rt
{
  template <typename T>
  struct vector2
  {
    static_assert(std::is_arithmetic_v<T>, "T must be an arithmetic type in vector2<T>");

    T x{0};
    T y{0};

    constexpr vector2() noexcept = default;
    constexpr explicit vector2(T x) noexcept : x{x}, y{x} {}
    constexpr vector2(T x, T y) noexcept : x{x}, y{y} {}

    template <typename U>
    constexpr explicit operator vector2<U>() const noexcept
    {
      return vector2
      {
        static_cast<U>(x),
        static_cast<U>(y)
      };
    }

    constexpr auto operator[](gsl::index i) const noexcept
    {
      Expects(0 <= i && i < 2);      
      
      if (i == 0)
        return x;
      else
        return y;
    }

    constexpr auto& operator[](gsl::index i) noexcept
    {
      Expects(0 <= i && i < 2);

      if (i == 0)
        return x;
      else
        return y;
    }
  };

  using vector2f = vector2<float>;
  using vector2i = vector2<int>;

  template <typename T>
  constexpr auto operator==(vector2<T> v1, vector2<T> v2) noexcept
  {
    return v1.x == v2.x && v1.y == v2.y;
  }

  template <typename T>
  constexpr auto operator!=(vector2<T> v1, vector2<T> v2) noexcept
  {
    return !(v1 == v2);
  }

  template <typename T, typename U>
  constexpr auto operator+(vector2<T> v1, vector2<U> v2) noexcept
  {
    return vector2{v1.x + v2.x, v1.y + v2.y};
  }

  template <typename T, typename U>
  constexpr auto& operator+=(vector2<T>& v1, vector2<T> v2) noexcept
  {
    v1.x += v2.x;
    v1.y += v2.y;
    return v1;
  }

  template <typename T, typename U>
  constexpr auto operator-(vector2<T> v1, vector2<T> v2) noexcept
  {
    return vector2{v1.x - v2.x, v1.y - v2.y};
  }

  template <typename T, typename U>
  constexpr auto& operator-=(vector2<T>& v1, vector2<T> v2) noexcept
  {
    v1.x -= v2.x;
    v1.y -= v2.y;
    return v1;
  }

  template <typename T, typename U>
  constexpr auto operator*(T s, vector2<U> v) noexcept
  {
    return vector2{s * v.x, s * v.y};
  }

  template <typename T, typename U>
  constexpr auto operator*(vector2<T> v, U s) noexcept
  {
    return vector2{v.x * s, v.y * s};
  }

  template <typename T, typename U>
  constexpr auto& operator*=(vector2<T>& v, U s) noexcept
  {
    v.x *= s;
    v.y *= s;
    return v;
  }

  template <typename T, typename U>
  constexpr auto operator/(vector2<T> v, U s) noexcept
  {
    Expects(s != 0);

    using result_t = decltype(v.x / s);
    
    if constexpr (std::is_integral_v<result_t>)
      return vector2{v.x / s, v.y / s};
    
    if constexpr (std::is_floating_point_v<result_t>)
    {
      auto inv{1.0f / s};
      return vector2{v.x * inv, v.y * inv};
    }
  }

  template <typename T, typename U>
  constexpr auto& operator/=(vector2<T>& v, U s) noexcept
  {
    Expects(s != 0);
    
    if constexpr (std::is_integral_v<T>)
    {
      v.x /= s;
      v.y /= s;
      return v;
    }
    
    if constexpr (std::is_floating_point_v<T>)
    {
      auto inv{1.0f / s};
      v.x *= inv;
      v.y *= inv;
      return v;
    }
  }

  template <typename T>
  constexpr auto operator-(vector2<T> v) noexcept
  {
    return vector2{-v.x, -v.y};
  }

  template <typename T>
  inline auto abs(vector2<T> v) noexcept
  {
    return vector2{std::abs(v.x), std::abs(v.y)};
  }

  template <typename T, typename U>
  constexpr auto dot(vector2<T> v1, vector2<U> v2) noexcept
  {
    return v1.x * v2.x + v1.y * v2.y;
  }

  template <typename T>
  constexpr auto length_sq(vector2<T> v) noexcept
  {
    return v.x * v.x + v.y * v.y;
  }

  template <typename T>
  inline auto length(vector2<T> v) noexcept
  {
    return std::sqrt(static_cast<float>(length_sq(v)));
  }

  template <typename T>
  inline auto normalize(vector2<T> v) noexcept
  {
    return v / length(v);
  }

  template <typename T>
  constexpr auto min_component(vector2<T> v) noexcept
  {
    return std::min(v.x, v.y);
  }

  template <typename T>
  constexpr auto max_component(vector2<T> v) noexcept
  {
    return std::max(v.x, v.y);
  }

  template <typename T>
  constexpr auto min_index(vector2<T> v) noexcept
  {
    return v.x < v.y ? 0 : 1;
  }

  template <typename T>
  constexpr auto max_index(vector2<T> v) noexcept
  {
    return v.x > v.y ? 0 : 1;
  }

  template <typename T>
  constexpr auto min(vector2<T> v1, vector2<T> v2) noexcept
  {
    return vector2{std::min(v1.x, v2.x), std::min(v1.y, v2.y)};
  }

  template <typename T>
  constexpr auto max(vector2<T> v1, vector2<T> v2) noexcept
  {
    return vector2{std::max(v1.x, v2.x), std::max(v1.y, v2.y)};
  }

  template <typename T>
  constexpr auto permute(vector2<T> v, gsl::index x, gsl::index y) noexcept
  {
    return vector2{v[x], v[y]};
  }
}

#endif