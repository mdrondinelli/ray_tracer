#ifndef GEOM_NORMAL3_HPP
#define GEOM_NORMAL3_HPP

#include <cmath>

#include <algorithm>
#include <type_traits>

#include <gsl/gsl>

#include <geom/vector3.hpp>

namespace rt
{
  template <typename T>
  struct normal3
  {
    static_assert(std::is_arithmetic_v<T>, "T must be an arithmetic type in normal3<T>");

    T x{0};
    T y{0};
    T z{0};

    constexpr normal3() noexcept = default;
    constexpr normal3(T x, T y, T z) noexcept : x{x}, y{y}, z{z} {}

    template <typename U>
    constexpr explicit operator normal3<U>() const noexcept
    {
      return normal3<U>
      {
        static_cast<U>(x),
        static_cast<U>(y),
        static_cast<U>(z)
      };
    }

    constexpr auto operator[](gsl::index i) const noexcept
    {
      Expects(0 <= i && i < 3);

      if (i == 0)
        return x;
      if (i == 1)
        return y;

      return z;
    }

    constexpr auto& operator[](gsl::index i) const
    {
      Expects(0 <= i && i < 3);

      if (i == 0)
        return x;
      if (i == 1)
        return y;
      
      return z;
    }
  };

  using normal3f = normal3<float>;

  template <typename T>
  constexpr auto to_normal(vector3<T> v) noexcept
  {
    return normal3{v.x, v.y, v.z};
  }

  template <typename T>
  constexpr auto to_vector(normal3<T> n) noexcept
  {
    return vector3{n.x, n.y, n.z};
  }

  template <typename T>
  constexpr auto operator==(normal3<T> n1, normal3<T> n2) noexcept
  {
    return n1.x == n2.x && n1.y == n2.y && n1.z == n2.z;
  }

  template <typename T>
  constexpr auto operator!=(normal3<T> n1, normal3<T> n2) noexcept
  {
    return !(n1 == n2);
  }

  template <typename T, typename U>
  constexpr auto operator+(normal3<T> n1, normal3<U> n2) noexcept
  {
    return normal3{n1.x + n2.x, n1.y + n2.y, n1.z + n2.z};
  }

  template <typename T, typename U>
  constexpr auto& operator+=(normal3<T>& n1, normal3<U> n2) noexcept
  {
    n1.x += n2.x;
    n1.y += n2.y;
    n1.z += n2.z;
    return n1;
  }

  template <typename T, typename U>
  constexpr auto operator-(normal3<T> n1, normal3<U> n2) noexcept
  {
    return normal3{n1.x - n2.x, n1.y - n2.y, n1.z - n2.z};
  }

  template <typename T, typename U>
  constexpr auto& operator-=(normal3<T>& n1, normal3<U> n2) noexcept
  {
    n1.x -= n2.x;
    n1.y -= n2.y;
    n1.z -= n2.z;
    return n1;
  }

  template <typename T, typename U>
  constexpr auto operator*(T s, normal3<U> n) noexcept
  {
    return normal3{s * n.x, s * n.y, s * n.z};
  }

  template <typename T, typename U>
  constexpr auto operator*(normal3<T> n, U s) noexcept
  {
    return normal3{n.x * s, n.y * s, n.z * s};
  }

  template <typename T, typename U>
  constexpr auto& operator*=(normal3<T>& n, U s) noexcept
  {
    n.x *= s;
    n.y *= s;
    n.z *= s;
    return n;
  }

  template <typename T, typename U>
  constexpr auto operator/(normal3<T> n, U s) noexcept
  {
    Expects(s != 0);

    using result_t = decltype(v.x / s);

    if constexpr (std::is_integral_v<result_t>)
      return normal3{n.x / s, n.y / s, n.z / s};

    if constexpr (std::is_floating_point_v<result_t>)
    {
      auto inv{1.0f / s};
      return normal3{n.x * inv, n.y * inv, n.z * inv};
    }
  }

  template <typename T, typename U>
  constexpr auto& operator/=(normal3<T>& n, U s) noexcept
  {
    Expects(s != 0);

    if constexpr (std::is_integral_v<T>)
    {
      n.x /= s;
      n.y /= s;
      n.z /= s;
      return n;
    }

    if constexpr (std::is_floating_point_v<T>)
    {
      auto inv{1.0f / s};
      n.x *= inv;
      n.y *= inv;
      n.z *= inv;
      return n;
    }
  }

  template <typename T>
  constexpr auto operator-(normal3<T> n) noexcept
  {
    return normal3{-n.x, -n.y, -n.z};
  }

  template <typename T>
  inline auto abs(normal3<T> n) noexcept
  {
    return normal3{std::abs(n.x), std::abs(n.y), std::abs(n.z)};
  }

  template <typename T, typename U>
  constexpr auto dot(normal3<T> n1, normal3<U> n2) noexcept
  {
    return n1.x * n2.x + n1.y * n2.y + n1.z * n2.z;
  }

  template <typename T, typename U>
  constexpr auto dot(normal3<T> n, vector3<U> v) noexcept
  {
    return n.x * v.x + n.y * v.y + n.z * v.z;
  }

  template <typename T, typename U>
  constexpr auto dot(vector3<T> v, normal3<U> n) noexcept
  {
    return n.x * v.x + n.y * v.y + n.z * v.z;
  }

  template <typename T>
  constexpr auto length_sq(normal3<T> n) noexcept
  {
    return n.x * n.x + n.y * n.y + n.z * n.z;
  }

  template <typename T>
  inline auto length(normal3<T> n) noexcept
  {
    return std::sqrt(static_cast<float>(length_sq(n)));
  }

  template <typename T>
  inline auto normalize(normal3<T> n) noexcept
  {
    return n / length(n);
  }

  template <typename T, typename U>
  constexpr auto face_forward(normal3<T> n1, normal3<U> n2) noexcept
  {
    return dot(n1, n2) < 0 ? -n1 : n1;
  }

  template <typename T, typename U>
  constexpr auto face_forward(vector3<T> v1, vector3<U> v2) noexcept
  {
    return dot(v1, v2) < 0 ? -v1 : v1;
  }

  template <typename T, typename U>
  constexpr auto face_forward(normal3<T> n, vector3<U> v) noexcept
  {
    return dot(n, v) < 0 ? -n : n;
  }

  template <typename T, typename U>
  constexpr auto face_forward(vector3<T> v, normal3<U> n) noexcept
  {
    return dot(v, n) < 0 ? -v : v;
  }

  template <typename T>
  constexpr auto min_component(normal3<T> n) noexcept
  {
    return std::min(n.x, std::min(n.y, n.z));
  }

  template <typename T>
  constexpr auto max_component(normal3<T> n) noexcept
  {
    return std::max(n.x, std::max(n.y, n.z));
  }

  template <typename T>
  constexpr auto min_dimension(normal3<T> n) noexcept
  {
    return n.x < n.y ? (n.x < n.z ? 0 : 2) : (n.y < n.z ? 1 : 2);
  }

  template <typename T>
  constexpr auto max_dimension(normal3<T> n) noexcept
  {
    return n.x > n.y ? (n.x > n.z ? 0 : 2) : (n.y > n.z ? 1 : 2);
  }
  
  template <typename T>
  constexpr auto min(normal3<T> n1, normal3<T> n2) noexcept
  {
    return normal3{std::min(n1.x, n2.x), std::min(n1.y, n2.y), std::min(n1.z, n2.z)};
  }

  template <typename T>
  constexpr auto max(normal3<T> n1, normal3<T> n2) noexcept
  {
    return normal3{std::max(n1.x, n2.x), std::max(n1.y, n2.y), std::max(n1.z, n2.z)};
  }
}

#endif