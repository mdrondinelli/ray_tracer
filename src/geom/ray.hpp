#ifndef GEOM_RAY_HPP
#define GEOM_RAY_HPP

#include <geom/point3.hpp>
#include <geom/vector3.hpp>
#include <util/constants.hpp>

namespace rt
{
  struct ray
  {
    point3f o;
    vector3f d;
    float tmax{inf_val<float>};

    float time{0.0f};

    constexpr ray() noexcept = default;
    constexpr ray(point3f o, vector3f d, float tmax = inf_val<float>, float time = 0.0f) noexcept
      : o{o}, d{d}, tmax{tmax}, time{time} {}

    constexpr auto operator()(float t) noexcept { return o + t * d; }
  };

  constexpr auto operator==(const ray& r1, const ray& r2) noexcept
  {
    return r1.time == r2.time && r1.tmax == r2.tmax && r1.o == r2.o && r1.d == r2.d;
  }

  constexpr auto operator!=(const ray& r1, const ray& r2) noexcept
  {
    return !(r1 == r2);
  }
}

#endif