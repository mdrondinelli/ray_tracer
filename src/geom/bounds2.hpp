#ifndef GEOM_BOUNDS2_HPP
#define GEOM_BOUNDS2_HPP

#include <type_traits>
#include <utility>

#include <gsl/gsl>

#include <geom/point2.hpp>
#include <geom/utility.hpp>
#include <geom/vector2.hpp>
#include <util/constants.hpp>

namespace rt
{
  template <typename T>
  struct bounds2
  {
    static_assert(std::is_arithmetic_v<T>, "T must be an arithmetic type in bounds2<T>");

    class iterator
    {
      static_assert(std::is_integral_v<T>, "T must be an integral type in bounds2<T>::iterator");
    public:
      inline iterator(const bounds2<T>& bounds, point2<T> offset) noexcept : bounds_{&bounds}, offset_{offset} {}

      inline friend auto operator==(const iterator& i1, const iterator& i2) noexcept
      {
        return i1.bounds_ == i2.bounds_ && i1.offset_ == i2.offset_;
      }

      inline friend auto operator!=(const iterator& i1, const iterator& i2) noexcept
      {
        return !(i1 == i2);
      }

      inline auto operator*() const noexcept
      {
        return offset_;
      }

      inline auto& operator++() noexcept
      {
        if (++offset_.x == bounds_->p_max.x)
        {
          offset_.x = bounds_->p_min.x;
          offset_.y++;
        }

        return *this;
      }
    private:
      const bounds2<T>* bounds_{nullptr};
      point2<T> offset_;
    };

    point2<T> p_min{max_val<T>};
    point2<T> p_max{low_val<T>};

    constexpr bounds2() noexcept = default;
    constexpr explicit bounds2(point2<T> p) noexcept : p_min{p}, p_max{p} {}
    constexpr bounds2(point2<T> p1, point2<T> p2) noexcept : p_min{min(p1, p2)}, p_max{max(p1, p2)} {}

    constexpr auto operator[](gsl::index i) const noexcept
    {
      Expects(0 <= i && i < 2);

      if (i == 0)
        return p_min;
      else
        return p_max;
    }

    constexpr auto& operator[](gsl::index i) noexcept
    {
      Expects(0 <= i && i < 2);

      if (i == 0)
        return p_min;
      else
        return p_max;
    }

    inline auto begin() const noexcept
    {
      return iterator{*this, p_min};
    }

    inline auto end() const noexcept
    {
      return iterator{*this, point2<T>{p_min.x, p_max.y}};
    }
  };

  using bounds2f = bounds2<float>;
  using bounds2i = bounds2<int>;

  template <typename T>
  constexpr auto operator==(const bounds2<T>& b1, const bounds2<T>& b2) noexcept
  {
    return b1.p_min == b2.p_min && b1.p_max == b2.p_max;
  }

  template <typename T>
  constexpr auto operator!=(const bounds2<T>& b1, const bounds2<T>& b2) noexcept
  {
    return !(b1 == b2);
  }

  template <typename T>
  constexpr auto corner(const bounds2<T>& b, gsl::index i) noexcept
  {
    Expects(0 <= i && i < 4);

    return point2
    {
      b[i >> 0 & 1].x,
      b[i >> 1 & 1].y
    };
  }

  template <typename T>
  constexpr auto union_of(const bounds2<T>& b, point2<T> p) noexcept
  {
    return bounds2
    {
      min(b.p_min, p),
      max(b.p_max, p)
    };
  }

  template <typename T>
  constexpr auto union_of(const bounds2<T>& b1, const bounds2<T>& b2) noexcept
  {
    return bounds2
    {
      min(b1.p_min, b2.p_min),
      max(b1.p_max, b2.p_max)
    };
  }

  template <typename T>
  constexpr auto intersects(const bounds2<T>& b1, const bounds2<T>& b2) noexcept
  {
    return
      b1.p_max.x >= b2.p_min.x && b1.p_min.x <= b2.p_max.x &&
      b1.p_max.y >= b2.p_min.y && b1.p_min.y <= b2.p_max.y;
  }

  template <typename T>
  constexpr auto intersect_of(const bounds2<T>& b1, const bounds2<T>& b2) noexcept
  {
    Expects(intersects(b1, b2));

    return bounds2
    {
      max(b1.p_min, b2.p_min),
      min(b1.p_max, b2.p_max)
    };
  }

  template <typename T>
  constexpr auto inside_inclusive(const bounds2<T>& b, point2<T> p) noexcept
  {
    return
      b.min_p.x <= p.x && p.x <= b.max_p.x &&
      b.min_p.y <= p.y && p.y <= b.max_p.y;
  }

  template <typename T>
  constexpr auto inside_exclusive(const bounds2<T>& b, point2<T> p) noexcept
  {
    return
      b.min_p.x <= p.x && p.x < b.max_p.x &&
      b.min_p.y <= p.y && p.y < b.max_p.y;
  }

  template <typename T>
  constexpr auto grow(const bounds2<T>& b, T delta) noexcept
  {
    vector2 vec{delta};
    return bounds2
    {
      b.p_min - vec,
      b.p_max + vec
    };
  }

  template <typename T>
  constexpr auto diagonal(const bounds2<T>& b) noexcept
  {
    return b.p_max - b.p_min;
  }

  template <typename T>
  constexpr auto area(const bounds2<T>& b) noexcept
  {
    auto d{diagonal(b)};
    return d.x * d.y;
  }

  template <typename T>
  constexpr auto min_component(const bounds2<T>& b) noexcept
  {
    return min_component(diagonal(b));
  }

  template <typename T>
  constexpr auto max_component(const bounds2<T>& b) noexcept
  {
    return max_component(diagonal(b));
  }

  template <typename T>
  constexpr auto min_dimension(const bounds2<T>& b) noexcept
  {
    return min_dimension(diagonal(b));
  }

  template <typename T>
  constexpr auto max_dimension(const bounds2<T>& b) noexcept
  {
    return max_dimension(diagonal(b));
  }

  template <typename T>
  constexpr auto lerp(const bounds2<T>& b, point2f t) noexcept
  {
    return point2
    {
      lerp(b.p_min.x, b.p_max.x, t.x),
      lerp(b.p_min.y, b.p_max.y, t.y)
    };
  }

  template <typename T>
  constexpr auto lerp_inv(const bounds2<T>& b, point2<T> p) noexcept
  {
    return point2f
    {
      lerp_inv(b.p_min.x, b.p_max.x, p.x),
      lerp_inv(b.p_min.y, b.p_max.y, p.y)
    };
  }

  template <typename T>
  inline auto bounding_sphere(const bounds2<T>& b) noexcept
  {
    auto center{midpoint(static_cast<point2f>(b.p_min), static_cast<point2f>(b.p_max))};
    auto radius{distance(center, b.p_max)};
    return std::pair{center, radius};
  }
}

#endif