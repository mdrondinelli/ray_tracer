#include <chrono>
#include <iostream>
#include <memory>
#include <utility>

#include <geom/bounds2.hpp>
#include <geom/bounds3.hpp>
#include <geom/matrix4x4.hpp>
#include <geom/point2.hpp>
#include <geom/point3.hpp>
#include <geom/ray_differential.hpp>
#include <geom/ray.hpp>
#include <geom/utility.hpp>
#include <geom/vector2.hpp>
#include <geom/vector3.hpp>

#include <util/stack_allocator.hpp>
#include <util/tiled_array.hpp>

auto test()
{
}

int main()
{
  using namespace rt;

  // auto [x0, x1] = solve_quadratic(1, 6, 9).value();

  // std::cout << x0 << std::endl;
  // std::cout << x1 << std::endl;

  return 0;
}