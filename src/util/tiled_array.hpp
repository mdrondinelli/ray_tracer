#ifndef UTIL_TILED_ARRAY
#define UTIL_TILED_ARRAY

#include <vector>
#include <gsl/gsl>

namespace rt
{
  template <typename T>
  class tiled_array
  {
  public:
    template <typename U>
    inline friend void swap(tiled_array<U>& arr1, tiled_array<U>& arr2) noexcept;

    using value_type = typename std::vector<T>::value_type;

    using reference = typename std::vector<T>::reference;
    using const_reference = typename std::vector<T>::const_reference;
    
    using iterator = typename std::vector<T>::iterator;
    using const_iterator = typename std::vector<T>::const_iterator;

    using difference_type = typename std::vector<T>::difference_type;
    using size_type = typename std::vector<T>::size_type;

    inline tiled_array(std::size_t tile_size, std::size_t u_size, std::size_t v_size)
      : tile_size_{tile_size}, u_size_{u_size}, v_size_{v_size}, data_(u_size * v_size)
    {
      Expects(tile_size != 0);
      Expects(u_size != 0);
      Expects(v_size != 0);
    }
    
    inline auto tile_of(std::size_t i) const noexcept
    {
      return i / tile_size_;
    }

    inline auto offs_of(std::size_t i) const noexcept
    {
      return i % tile_size_;
    }

    inline const auto& operator()(std::size_t u, std::size_t v) const noexcept
    {
      Expects(0 <= u && u < u_size_);
      Expects(0 <= v && v < v_size_);

      auto u_tile{tile_of(u)};
      auto u_offs{offs_of(u)};

      auto v_tile{tile_of(v)};
      auto v_offs{offs_of(v)};

      auto tiles_u{(u_size_ + tile_size_ - 1) / tile_size_};
      auto tile{tiles_u * v_tile + u_tile};

      auto offs{tile_size_ * tile_size_ * tile};
      offs += tile_size_ * v_offs + u_offs;

      return data_[offs];
    }

    inline auto& operator()(std::size_t u, std::size_t v) noexcept
    {
      Expects(0 <= u && u < u_size_);
      Expects(0 <= v && v < v_size_);

      auto u_tile{tile_of(u)};
      auto u_offs{offs_of(u)};

      auto v_tile{tile_of(v)};
      auto v_offs{offs_of(v)};

      auto tiles_u{(u_size_ + tile_size_ - 1) / tile_size_};
      auto tile{tiles_u * v_tile + u_tile};

      auto offs{tile_size_ * tile_size_ * tile};
      offs += tile_size_ * v_offs + u_offs;

      return data_[offs];
    }

    inline auto begin() noexcept { return data_.begin(); }
    inline auto end() noexcept { return data_.end(); }

    inline auto begin() const noexcept { return data_.begin(); }
    inline auto end() const noexcept { return data_.end(); }

    inline auto cbegin() const noexcept { return data_.cbegin(); }
    inline auto cend() const noexcept { return data_.cend(); }

    inline auto size() const noexcept { return data_.size(); }
    inline auto max_size() const noexcept { return data_.max_size(); }

    inline auto empty() const noexcept { return false; }

    inline auto get_tile_size() const noexcept { return tile_size_; }
    inline auto get_u_size() const noexcept { return u_size_; }
    inline auto get_v_size() const noexcept { return v_size_; }
  private:
    std::size_t tile_size_;
    std::size_t u_size_;
    std::size_t v_size_;

    std::vector<T> data_;
  };

  template <typename T>
  inline bool operator==(const tiled_array<T>& arr1, const tiled_array<T>& arr2) noexcept
  {
    return
      arr1.get_tile_size() == arr2.get_tile_size() &&
      arr1.get_u_size() == arr2.get_u_size() &&
      arr1.get_v_size() == arr2.get_v_size() &&
      std::equal(arr1.begin(), arr1.end(), arr2.begin(), arr2.end());
  }

  template <typename T>
  inline bool operator!=(const tiled_array<T>& arr1, const tiled_array<T>& arr2) noexcept
  {
    return !(arr1 == arr2);
  }

  template <typename T>
  inline void swap(tiled_array<T>& arr1, tiled_array<T>& arr2) noexcept
  {
    using std::swap;
    swap(arr1.tile_size_, arr2.tile_size_);
    swap(arr1.u_size_, arr2.u_size_);
    swap(arr1.v_size_, arr2.v_size_);
    swap(arr1.data_, arr2.data_);
  }
}

#endif