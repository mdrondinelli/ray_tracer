#ifndef UTIL_DELETERS_HPP
#define UTIL_DELETERS_HPP

#include <cstddef>
#include <type_traits>

namespace rt
{
  template <typename T>
  struct basic_placement_deleter
  {
    inline auto operator()(T* p)
    {
      p->~T();
    }
  };

  template <typename T>
  struct array_placement_deleter
  {
    std::size_t size;

    inline explicit array_placement_deleter(std::size_t size) noexcept
      : size{size} {}

    inline auto operator()(T* p)
    {
      for (std::size_t i{0}; i < size; ++i)
        p[i].~T();
    }
  };
}

#endif