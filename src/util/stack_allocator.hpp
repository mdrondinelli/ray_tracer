#ifndef UTIL_STACK_ALLOCATOR_HPP
#define UTIL_STACK_ALLOCATOR_HPP

#include <cstddef>
#include <cstdlib>

#include <memory>
#include <type_traits>
#include <vector>

#include <gsl/gsl>

#include <util/deleters.hpp>

namespace rt
{
  class stack
  {
  public:
    inline explicit stack(std::size_t size)
      : size_{size}, data_{std::make_unique<std::byte[]>(size)} {}
  
    inline auto alloc(std::size_t size) noexcept -> void*
    {
      if (offs_ + size > size_)
        return nullptr;
      
      auto ret{&data_[offs_]};
      offs_ += size;

      return ret;
    }
  private:
    std::size_t size_;
    std::size_t offs_{0};
    std::unique_ptr<std::byte[]> data_;
  };

  class stack_allocator
  {
  public:
    template <typename T>
    using basic_unique_ptr = std::unique_ptr<T, basic_placement_deleter<T>>;
    
    template <typename T>
    using array_unique_ptr = std::unique_ptr<T, array_placement_deleter<std::remove_extent_t<T>>>;

    inline explicit stack_allocator(std::size_t stack_size = 256 * 1024)
      : stack_size_{stack_size}, curr_stack_{stack_size_} {}
    
    inline auto alloc(std::size_t size)
    {
      constexpr auto alignment{alignof(std::max_align_t) - 1};
      
      size += alignment;
      size &= ~alignment;

      auto ret = curr_stack_.alloc(size);
      if (!ret)
      {
        dead_stacks_.emplace_back(std::move(curr_stack_));
        curr_stack_ = stack{std::max(stack_size_, size)};
        ret = curr_stack_.alloc(size);
      }

      return ret;
    }

    template <typename T, typename... Args>
    inline auto make_unique(Args&&... args) -> std::enable_if_t<!std::is_array_v<T>, basic_unique_ptr<T>>
    {
      auto ptr{alloc(sizeof(T))};
      auto ret{new (ptr) T(std::forward<Args>(args)...)};

      return std::unique_ptr<T, basic_placement_deleter<T>>{ret, basic_placement_deleter<T>{}};
    }

    template <typename T>
    inline auto make_unique(std::size_t size) -> std::enable_if_t<std::is_array_v<T>, array_unique_ptr<T>>
    {
      using elem_t = std::remove_extent_t<T>;

      auto ptr{alloc(sizeof(elem_t) * size)};
      auto ret{new (ptr) elem_t[size]};

      return std::unique_ptr<T, array_placement_deleter<elem_t>>{ret, array_placement_deleter<elem_t>{size}};
    }

    template <typename T, typename... Args>
    inline auto make_shared(Args&&... args) -> std::enable_if_t<!std::is_array_v<T>, std::shared_ptr<T>>
    {
      auto ptr{alloc(sizeof(T))};
      auto ret{new (ptr) T(std::forward<Args>(args)...)};

      return std::shared_ptr<T>{ret, basic_placement_deleter<T>{}};
    }

    template <typename T, typename... Args>
    inline auto make_shared(std::size_t size) -> std::enable_if_t<std::is_array_v<T>, std::shared_ptr<T>>
    {
      using elem_t = std::remove_extent_t<T>;

      auto ptr{alloc(sizeof(elem_t) * size)};
      auto ret{new (ptr) elem_t[size]};

      return std::shared_ptr<T>{ret, array_placement_deleter<elem_t>{size}};
    }
  private:
    const std::size_t stack_size_;

    std::vector<stack>  dead_stacks_;
    stack               curr_stack_;
  };
}

#endif