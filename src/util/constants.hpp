#ifndef UTIL_CONSTANTS_HPP
#define UTIL_CONSTANTS_HPP

#include <limits>

namespace rt
{
  template <typename T> constexpr T low_val{std::numeric_limits<T>::lowest()};
  template <typename T> constexpr T min_val{std::numeric_limits<T>::min()};
  template <typename T> constexpr T max_val{std::numeric_limits<T>::max()};
  template <typename T> constexpr T inf_val{std::numeric_limits<T>::infinity()};

  constexpr auto e{2.71828182845904523536f};
  constexpr auto pi{3.14159265358979323846f};
}

#endif